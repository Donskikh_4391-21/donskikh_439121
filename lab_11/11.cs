﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_11
{ 
    class Vector2D
{
    double x;
    double y;

    public Vector2D()
    {
        x = 0;
        y = 0;
    }

    public Vector2D(double x, double y)
    {
        this.x = x;
        this.y = y;
    }
    public Vector2D add(Vector2D a)
    {
        Vector2D temp = new Vector2D();
        temp.x = this.x + a.x;
        temp.y = this.y + a.y;

        return temp;
    }
    public Vector2D add2(Vector2D a)
    {
        Vector2D temp = new Vector2D();
        temp.x = this.x + a.x;
        temp.y = this.y + a.y;

        return temp;
    }
    public Vector2D sub(Vector2D a)
    {
        Vector2D temp = new Vector2D();
        temp.x = this.x - a.x;
        temp.y = this.y - a.y;

        return temp;
    }
    public Vector2D sub2(Vector2D a)
    {
        Vector2D temp = new Vector2D();
        temp.x = this.x - a.x;
        temp.y = this.y - a.y;

        return temp;
    }
    public Vector2D mult(double s)
    {
        Vector2D temp = new Vector2D();
        temp.x = this.x * s;
        temp.y = this.y * s;

        return temp;
    }
    public Vector2D ToString(Vector2D a)
    {
        Convert.ToString(a);
        return a;
    }
    public double length()
    {
        double dlina = Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2));
        return dlina;
    }
    public double scalarProduct(Vector2D a)
    {
        double scalarProduct = a.x * this.x + a.y * this.y;
        return scalarProduct;
    }
    public double cos(Vector2D a)
    {
        double cos = (a.x * this.x + a.y * this.y) / Math.Sqrt(Math.Pow(a.x, 2) + Math.Pow(a.y, 2)) + Math.Sqrt(Math.Pow(this.x, 2) + Math.Pow(this.y, 2));
        return cos;
    }
    public string equals(Vector2D a)
    {
        string text;
        double dlina_a = Math.Sqrt(Math.Pow(a.x, 2) + Math.Pow(a.y, 2));
        double dlina_this = Math.Sqrt(Math.Pow(this.x, 2) + Math.Pow(this.y, 2));

        if (dlina_a > dlina_this) text = "вектор obj3 длиннее"; else text = "вектор obj2 длиннее";
        return text;
    }
}
    class RationalVector2D
    {

        RationalFraction k_x; //дробь и 1 координата
        RationalFraction k_y; //дробь и 2 координата

        public RationalVector2D()
        {
            k_x = new RationalFraction();
            k_y = new RationalFraction();
        }
        public RationalVector2D(RationalFraction drob1, RationalFraction drob2)
        {
            k_x = drob1;
            k_y = drob2;
        }
        public RationalVector2D add(RationalVector2D a)
        {
            RationalVector2D add_vector = new RationalVector2D();
            add_vector.k_x = a.k_x.add2(k_x);
            add_vector.k_y = a.k_y.add2(k_y);
            return add_vector;
        }
        public String toString(RationalVector2D a)
        {
            string text = Convert.ToString(a);
            return text;
        }
        public double length(RationalVector2D a)
        {
            double dlina = Math.Sqrt(Math.Pow(a.k_x.value(), 2) + Math.Pow(a.k_y.value(), 2));
            return dlina;
        }
        public RationalFraction scalarProduct(RationalVector2D a)
        {
            RationalFraction scalarProduct = new RationalFraction();
            scalarProduct = a.k_x.sub2(this.k_x).add2(a.k_y.sub2(this.k_y));
            return scalarProduct;
        }
        public bool equals(RationalVector2D a)
        {
            RationalVector2D b = new RationalVector2D(k_x, k_y);
            if (length(a) > length(b))
                return true;
            else
                return false;
        }
    }
    class RationalFraction
    {
        double x;
        double y;

        public RationalFraction()
        {
            x = 0;
            y = 0;
        }
        public RationalFraction(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
        public RationalFraction reduce()
        {
            RationalFraction drob = new RationalFraction();
            if ((x / y) - Math.Truncate(x / y) == 0)
                drob.x = x / y;
            drob.y = 1;
            if ((y / x) - Math.Truncate(y / x) == 0)
                drob.x = 1;
            drob.y = y / x;
            return drob;
        }
        public RationalFraction add(RationalFraction a)
        {
            RationalFraction drob = new RationalFraction();
            drob.x = this.x * a.y + a.x * this.y;
            drob.y = this.y * a.y;
            drob = drob.reduce();
            return drob;
        }
        public RationalFraction add2(RationalFraction a)
        {
            RationalFraction drob = new RationalFraction();
            drob.x = this.x * a.y + a.x * this.y;
            drob.y = this.y * a.y;
            drob = drob.reduce();
            return drob;
        }
        public RationalFraction sub(RationalFraction a)
        {
            RationalFraction drob = new RationalFraction();
            drob.x = this.x * a.y - a.x * this.y;
            drob.y = this.y * a.y;
            drob = drob.reduce();
            return drob;
        }
        public RationalFraction sub2(RationalFraction a)
        {
            RationalFraction drob = new RationalFraction();
            drob.x = this.x * a.y - a.x * this.y;
            drob.y = this.y * a.y;
            drob = drob.reduce();
            return drob;
        }
        public RationalFraction mult(RationalFraction a)
        {
            RationalFraction drob = new RationalFraction();
            drob.x = this.x * a.x;
            drob.y = this.y * a.y;
            drob = drob.reduce();
            return drob;
        }
        public RationalFraction mult2(RationalFraction a)
        {
            RationalFraction drob = new RationalFraction();
            drob.x = this.x * a.x;
            drob.y = this.y * a.y;
            drob = drob.reduce();
            return drob;
        }
        public RationalFraction div(RationalFraction a)
        {
            RationalFraction drob = new RationalFraction();
            drob.x = this.x * a.y;
            drob.y = this.y * a.x;
            drob = drob.reduce();
            return drob;
        }
        public RationalFraction div2(RationalFraction a)
        {
            RationalFraction drob = new RationalFraction();
            drob.x = this.x * a.y;
            drob.y = this.y * a.x;
            drob = drob.reduce();
            return drob;
        }
        public RationalFraction ToString2(RationalFraction a)
        {
            Convert.ToString(a);
            return a;
        }
        public double value()
        {
            double value = x / y;
            return value;
        }
        public string equals2(RationalFraction a)
        {
            string text;
            if (this.x / this.y > a.x / a.y) text = "Первая дробь больше"; else text = "Вторая дробь больше";
            return text;
        }
        public int numberPart()
        {
            int cel = 0;
            if (x / y > 1)
                cel = Convert.ToInt32(Math.Truncate(x / y));
            return cel;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            //Задание 1
            Vector2D obj1 = new Vector2D();
            Vector2D obj2 = new Vector2D(4.1, 6);
            Vector2D obj3 = new Vector2D(6, 1);

            Vector2D obj4 = obj2.add(obj3);
            obj2 = obj2.add2(obj3);

            Vector2D obj5 = obj2.sub(obj3);
            obj2 = obj2.sub2(obj3);

            Vector2D obj6 = obj2.mult(3.5);
            obj2 = obj2.mult(3.5);

            string String = obj2.ToString();

            double length = obj2.length();

            double scalarProduct = obj2.scalarProduct(obj3);

            double cos = obj2.cos(obj3);

            string equals =obj2.equals(obj3);
            Console.WriteLine(equals);

            //Задание 2
            RationalFraction drob = new RationalFraction();
            RationalFraction drob2 = new RationalFraction(2, 4);
            RationalFraction drob3 = new RationalFraction(1, 3);

            RationalFraction reduce = drob2.reduce();

            RationalFraction drob4 = drob2.add(drob3);
            drob2 = drob2.add2(drob3);

            RationalFraction drob5 = drob2.sub(drob3);
            drob2 = drob2.sub2(drob3);

            RationalFraction drob6 = drob2.mult(drob3);
            drob2 = drob2.mult2(drob3);

            RationalFraction drob7 = drob2.div(drob3);
            drob2 = drob2.div2(drob3);

            string String2 = drob2.ToString();

            double value = drob2.value();

            string equals2 = drob2.equals2(drob3);
            Console.WriteLine(equals);

            int numberPart = drob2.numberPart();

            //  ЛАБОРАТОРНАЯ 12, Задание 1

            RationalVector2D zero = new RationalVector2D();
            RationalVector2D vect1 = new RationalVector2D(drob2, drob3);
            RationalVector2D vect2 = new RationalVector2D(drob3, drob2);
            RationalVector2D summ = vect1.add(vect2);
            string texts = vect1.toString(vect1);
            double dlina = vect1.length(vect1);
            RationalFraction scalar = vect1.scalarProduct(vect2);
            bool equals1 = vect1.equals(vect2);
        }
    }
}
