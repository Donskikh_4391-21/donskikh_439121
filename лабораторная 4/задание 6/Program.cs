﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace задание_6
{
    class Program
    {
        static double f(double x)
        {
            if (x % 5 == 0)
            {
                x = x / 5;
                return x;
            }
            else
            {
                x = x + 1;
                return x;
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Пример:" + f(8));
            Console.WriteLine("Пример:" + f(15));
        }
    }
}
