﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace задание_4
{
    class Program
    {
        static double f(int n)
        {
            double r = Math.Sqrt(n) + n;
            return r;
        }
        static void Main(string[] args)
        {
            double r = f(6) / 2 + f(13) / 2 + f(21) / 2;
            Console.WriteLine("r= " + r);
        }
    }
}
