﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace блок2_задание1
{
    class Program
    {
        static double f(double x)
        {
            if (x >= 0.9)
            {
                return 1.0 / (0.1 + Math.Pow(x, 2));
            }
            else if (x >= 0 && x < 0.9)
            {
                return 0.2 * x + 0.1;
            }
            else
            {
                return Math.Pow(x, 2) + 0.2;
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Введите a:");
            double a = double.Parse(Console.ReadLine());
            Console.WriteLine("Введите b:");
            double b = double.Parse(Console.ReadLine());
            Console.WriteLine("Введите h:");
            double h = double.Parse(Console.ReadLine());
            Console.WriteLine("X Y");
            for (double x = a; x <= b; x += h)
            {
                Console.WriteLine(x + " " + f(x));
            }
        }
    }
}