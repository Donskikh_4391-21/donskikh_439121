﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace задание_2
{
    class Program
    {
        static double f(int x)
        {
            double z = Math.Pow(x, 3) - Math.Sin(x * Math.PI / 180);
            return z;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Введите a:");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите b:");
            int b = int.Parse(Console.ReadLine());
            if (f(a) > f(b))
            {
                Console.WriteLine("F(a) принимает большее значение");
            }
            else
            {
                Console.WriteLine("F(b) принимает большее значение");
            }
        }
    }
}
