﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace задание_5
{
    class Program
    {
        static double f(double x)
        {
            if (x % 2 == 0)
            {
                x = x / 2;
                return x;
            }
            else
            {
                x = 0;
                return x;
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Введите x");
            double x = Convert.ToInt32(Console.ReadLine());
            double z = f(x);
            Console.WriteLine("z=" + z);
        }
    }
}
