﻿using System;

namespace Lab7
{
    class Program
    {
        static void Enter(int [,] mas, int n, int m)
        {
            Random rnd = new Random();
            for (int i = 0; i < mas.GetLength(0); i++)
            {
                for (int j = 0; j < mas.GetLength(1); j++)
                {
                    mas[i, j] = rnd.Next(n, m);                   
                }
            }
        }

        static void Print(int[,] mas)
        {
            for (int i = 0; i < mas.GetLength(0); i++)
            {
                for (int j = 0; j < mas.GetLength(1); j++)
                {
                    Console.Write(mas[i, j] + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        static void Print(int[] mas)
        {
            for (int i = 0; i < mas.GetLength(0); i++)
            {
                 Console.Write(mas[i] + " ");
            }
            Console.WriteLine();
        }

        static void Print_n(int[,] mas, int b, int n)
        {
            for (int j = 0; j < b; j++)
            {
                    Console.Write(mas[n-1, j] + " ");
            }
            Console.WriteLine();
        }
        static void Print_m(int[,] mas, int b, int m)
        {
            for (int i = 0; i < b; i++)
            {
                Console.Write(mas[i, m-1] + " ");
            }
            Console.WriteLine();
        }
        static void SumStroki(int[,] mas, int b, int m)
        {
            int sum = 0;
            for (int j = 0; j < b; j++)
            {
                sum = sum + mas[m - 1, j];
            }
            Console.WriteLine("Сумма 3 строки = "+sum) ;
            Console.WriteLine();
        }

        static void Change(int[,] mas)
        {
            int k;
            for (int n = 0; n < mas.GetLength(0) - mas.GetLength(0) / 2; n++)
            {
                for (int j = 0; j < mas.GetLength(1); j++)
                {
                    k = mas[n, j];
                    mas[n, j] = mas[mas.GetLength(0) - 1 - n, j];
                    mas[mas.GetLength(0) - 1 - n, j] = k;
                }
            }
        }

        static int Info(int[,] mas, int b, int n)
        {
            int sum = 0;
            for (int j = 0; j < b; j++)
            {
                if (mas[n - 1, j] == 0)
                    sum = sum + 1;
            }
            return sum;
        }

        static int MinStrok(int[,] mas)
        {
            int sum = 0, min;
            for (int j = 0; j < mas.GetLength(1); j++)
            {
                sum = sum + mas[0, j];
            }
            min = sum;
            sum = 0;
            for (int i = 1; i < mas.GetLength(0); i++)
            {
                for (int j = 0; j < mas.GetLength(1); j++)
                {
                    sum = sum + mas[i, j];
                }                
                if (sum < min)
                    min = sum;
                sum = 0;
            }
            return min;
        }

        static int MaxStolb(int[,] mas)
        {
            int sum = 0;
            for (int i = 0; i < mas.GetLength(0); i++)
            {
                sum = sum + mas[i, 0];
            }            
            int max = sum;
            for (int j = 0; j < mas.GetLength(1); j++)
            {
                sum = 0;
                for (int i = 0; i < mas.GetLength(0); i++)
                {
                    sum = sum + mas[i, j];
                }
                if (sum > max)
                    max = sum;
            }
            return max;
        }

        static int Sum1(int[,] mas)
        {
            int sum = 0;
            for (int i = 0; i < mas.GetLength(0); i++)
            {
                for (int j = 0; j < mas.GetLength(1); j++)
                {
                    if (i == j)
                        sum = sum + mas[i, j];
                }
            }
            return sum;
        }

        static int Sum2(int[,] mas)
        {
            int sum = 0;
            for (int i = 0; i < mas.GetLength(0); i++)
            {
                for (int j = 0; j < mas.GetLength(1); j++)
                {
                    if (j == mas.GetLength(1) - 1 - i)
                        sum = sum + mas[i, j];
                }
            }
            return sum;
        }

        static int Sum_Positiv(int[,] mas, int k)
        {
            int sum = 0;
            for (int i = 0; i < mas.GetLength(0); i++)
            {
                    if (mas[i, k] % 2 == 0 && mas[i, k] > 0)
                        sum = sum + mas[i, k];
            }
            return sum;
        }

        static void NewOneDemArray (int [] mas, int[,] mas1)
        {
            for (int i = 0; i < mas.Length; i++)
            {
                mas[i] = Sum_Positiv(mas1, i);
            }
        }

        static int Modul(int[,] mas, int k)
        {
            int max = mas[1, 1];
            for (int i = 0; i < mas.GetLength(0); i++)
            {
                if (Math.Abs(mas[k, i]) > max)
                    max = mas[k, i];
            }
            return max;
        }

        static void NewTwoDemArray(int[] mas, int[,] mas1)
        {
            for (int i = 0; i < mas.Length; i++)
            {
                mas[i] = Modul(mas1, i);
            }
        }

        static void Enter_1(int[,] mas)
        {
            for (int i = 0; i < mas.GetLength(0); i++)
            {
                for (int j = 0; j < mas.GetLength(1); j++)
                {
                    if (i == j || j == mas.GetLength(1) - 1 - i)
                        mas[i, j] = 1;
                    else
                        mas[i, j] = 0;
                }
            }
        }

        static void Enter_2(int[,] mas)
        {
            int i = 0, k = 1, j = 0;
            for (j = 0; j < mas.GetLength(1); j++)        //1
            {
                mas[i, j] = k;
                k++;
            }
            j--;
            for (i = 1; i < mas.GetLength(0); i++)   //2
            {
                mas[i, j] = k;
                k++;
            }
            i--;
            for (j = 3; j >= 0; j--)                 //3
            {
                mas[i, j] = k;
                k++;
            }
            j++;
            for (i = 3; i > 0; i--)                  //4
            {
                mas[i, j] = k;
                k++;
            }
            i++;
            for (j=1; j < mas.GetLength(1) - 1; j++) //5
            {
                mas[i, j] = k;
                k++;
            }
            j--;
            for (i=2; i < mas.GetLength(0)- 1; i++)  //6
            {
                mas[i, j] = k;
                k++;
            }
            i--;
            for (j=2; j >= 1; j--)                   //7
            {
                mas[i, j] = k;
                k++;
            }
            j++;
            for (i=2; i > 1; i--)                    //8
            {
                mas[i, j] = k;
                k++;
            }
            i++;
            for (j=2; j < 3; j++) //9
            {
                mas[i, j] = k;
                k++;
            }
        }

        static void Main(string[] args)
        {
            int i, j, n, m;
            //ЗАДАЧА 1
            Console.WriteLine("Задача 1");
            Console.WriteLine("Введите размер массива.");
            Console.WriteLine("Введите кол-во строк:");
            i = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите кол-во столбцов:");
            j = int.Parse(Console.ReadLine());
            int[,] mas1 = new int[i,j] ;
            Enter(mas1, 0, 10);
            Print(mas1);
            Console.WriteLine("Введите номер строки:");
            n = int.Parse(Console.ReadLine());
            while (n <= 0 || n > i)
            {
                Console.WriteLine("В данном массиве не существует такая строка. Введите номер строки:");
                n = int.Parse(Console.ReadLine());
            }
            Print_n(mas1, j, n);
            Console.WriteLine("Введите номер столбца:");
            m = int.Parse(Console.ReadLine());
            while (m <= 0 || m > j)
            {
                Console.WriteLine("В данном массиве не существует такой столбец. Введите номер столбца:");
                m = int.Parse(Console.ReadLine());
            }
            Print_m(mas1, i, m);
            SumStroki(mas1, j, 3);

            Console.WriteLine();

            //ЗАДАЧА 2
            Console.WriteLine("Задача 2");
            Console.WriteLine("Введите размер массива.");
            Console.WriteLine("Введите кол-во строк:");
            i = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите кол-во столбцов:");
            j = int.Parse(Console.ReadLine());
            int[,] mas2 = new int[i, j];
            Enter(mas2, 0, 10);
            Console.WriteLine("Матрица:");
            Print(mas2);
            Console.WriteLine();            
            Change(mas2);
            Console.WriteLine("Новая матрица:");
            Print(mas2);

            Console.WriteLine();

            //ЗАДАЧА 3
            Console.WriteLine("Задача 3");
            int[,] Wagons = new int[18, 36];
            Enter(Wagons, 0, 2);
            Print(Wagons);
            Console.WriteLine();
            Console.WriteLine("Чтобы перестать узнавать кол-во мест введите 0");
            do
            {
                Console.WriteLine("Введите номер вагона:");
                n = int.Parse(Console.ReadLine());
                if (n == 0)
                    break;
                while (n < 0 || n > 18)
                {
                    Console.WriteLine("Такого вагона нет. Попробуйте ещё раз:");
                    n = int.Parse(Console.ReadLine());
                    if (n == 0)
                        break;                    
                }
                if (n == 0)
                    break;
                Console.WriteLine("Кол-во свободных мест:" + Info(Wagons, 36, n));
                Console.WriteLine();
            } while (n != 0);

            Console.WriteLine();

            //ЗАДАЧА 4
            Console.WriteLine("Задача 4");
            Console.WriteLine("Введите размер массива.");
            Console.WriteLine("Введите кол-во строк:");
            i = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите кол-во столбцов:");
            j = int.Parse(Console.ReadLine());
            int[,] mas4 = new int[i, j];
            Enter(mas4, 0, 10);
            Print(mas4);
            Console.WriteLine("Минимальная сумма строки: " + MinStrok(mas4));
            Console.WriteLine("Минимальная сумма столбца: " + MaxStolb(mas4));
            
            Console.WriteLine();

            //ЗАДАЧА 5
            Console.WriteLine("Задача 5");
            Console.WriteLine("Введите размер квадратного массива:");
            i = int.Parse(Console.ReadLine());
            j = i;
            int[,] mas5 = new int[i, j];
            Enter(mas5, 0, 10);
            Print(mas5);
            Console.WriteLine("Сумма главной диагонали:" + Sum1(mas5));
            Console.WriteLine("Сумма пабочной диагонали:" + Sum2(mas5));

            Console.WriteLine();

            //ЗАДАЧА 6
            Console.WriteLine("Задача 6");
            Console.WriteLine("Введите размер массива.");
            Console.WriteLine("Введите кол-во строк:");
            i = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите кол-во столбцов:");
            j = int.Parse(Console.ReadLine());
            int[,] mas6 = new int[i, j];
            Enter(mas6, -10, 10);
            Console.WriteLine("Матрица:");
            Print(mas6);
            int[] mas6_1 = new int[j];
            NewOneDemArray(mas6_1, mas6);
            Console.WriteLine("A) Новая матрица:");
            Print(mas6_1);
            int[] mas6_2 = new int[j];
            NewTwoDemArray(mas6_2, mas6);
            Console.WriteLine("Б) Новая матрица:");
            Print(mas6_2);

            Console.WriteLine();

            //ЗАДАЧА 7
            Console.WriteLine("Задача 7");
            int[,] mas7 = new int[7, 7];
            Enter_1(mas7);
            Print(mas7);

            int[,] mas7_1 = new int[5, 5];
            Enter_2(mas7_1);
            Print(mas7_1);
        }
    }
}
