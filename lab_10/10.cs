﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Lab_10
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1 
              Console.WriteLine("Введите осмысленное предложение");
              string input = Console.ReadLine();
              Console.WriteLine("Введите заданное слово");
              string pattern = Console.ReadLine();
                  if (Regex.IsMatch(input, pattern))
                      Console.WriteLine("Встречается");
                  else Console.WriteLine("Не встречается");
            // 2
            Console.WriteLine("Введите осмысленное предложение");
            string input2 = Console.ReadLine();
            Console.WriteLine("Введите заданную длину");
            int n = int.Parse(Console.ReadLine());
            string pattern2 = @"\b\w{"+n+@"}\b";
            foreach (Match match in Regex.Matches(input2, pattern2))
            {
                Console.WriteLine(match.Value);
            }
            // 3
            Console.WriteLine("Введите осмысленное предложение");
            string input3 = Console.ReadLine();
            string a = @"\b[а-я]\w\b";
            foreach (Match match in Regex.Matches(input3, a))
            {
              Console.WriteLine(match.Value);
            }
            // 4
            Console.WriteLine("Введите осмысленное предложение");
            string input4 = Console.ReadLine();
            string[] splitted = input3.Split(new[] { '-', '.', '?', '!', ')', '(', ',', ':' }, StringSplitOptions.RemoveEmptyEntries);
            char symbol = 'a';
            foreach (string s in splitted)
            {
                if (s.Contains(symbol))
                {
                    input4 = input4.Replace(s, string.Empty);
                }
            }
            Console.WriteLine(input3);
            // 5
            Console.WriteLine("Введите осмысленное предложение");
            string input5 = Console.ReadLine();
            var En = @"[a-zA-Z]+";
            var str2 = Regex.Replace(input5, En, "...");
            Console.WriteLine(str2);
            // 6
            Console.WriteLine("Введите осмысленное предложение");
            string input6 = Console.ReadLine();
            double sum = Regex.Matches(Console.ReadLine(), @"\d*\.?\d+").Cast<Match>().Select(m => m.Value).Select(double.Parse).Sum();
            Console.WriteLine(sum);
            // 7
            Console.WriteLine("Введите осмысленное предложение");
            string str = Console.ReadLine();

            char delim = ' ';
            string[] arr = str.Split(delim);

            Console.WriteLine(str);

            Regex[] patterns = {
               new Regex("(^\\d{3}-\\d{3}-\\d{2})"),
               new Regex("(^\\d{2}-\\d{2}-\\d{2})"),
            };

            for (int i = 0; i < arr.Length; i++)
            {
                foreach (var pat in patterns)
                {
                    if (pat.IsMatch(arr[i]))
                    {
                        Console.WriteLine(arr[i]);
                    }
                }
            }
            // 8
            Console.WriteLine("введите дату в формате дата.месяц.год");
            string text = Convert.ToString(Console.ReadLine());
        
            string pattern8 = "[0-3][0-9].[0-1][0-9].[1,2][9,0][0-9][0-9]";
           
            MatchCollection matches;
            Regex reg = new Regex(pattern8);
            matches = reg.Matches(text);
            
            {
                for (int i = 0; i < matches.Count; i++)
                { 
                    string updDate = DateTime.Parse(matches[i].Value).AddDays(-1).ToShortDateString();
                    text = text.Replace(matches[i].Value, updDate);
                }
                Console.WriteLine("дата предыдущенго дня {0}", text);
            }
            // 9
            Regex t = new Regex(@"(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})");
            string text9 = @" ip:123.231.121.223 , ip:234.143.216.123 , ip:157.216.106.164";
            int check = int.Parse(Console.ReadLine());
            string[] str9 = text9.Split(',');
            foreach (string s in str9)
            {
                Match ip = t.Match(s);
                if (ip.Success)
                {
                    string[] split = s.Split('.');
                    int p = int.Parse(split[3]); 
                    {
                        if (p / 100 != check) 
                            Console.WriteLine("Итог: " + ip);
                    }
                }

            }




        }
    }
}
