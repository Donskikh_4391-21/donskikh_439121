﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cr3
{

    class Zapis
    {

        private String _famil;

        private String _name;

        private String _telefon;

        private int[] _date = new int[3];

        private static int _KolZap;


        public Zapis()
        {

            Famil = "";

            Name = "";

            Telefon = "";

            Date[0] = 0;

            Date[1] = 0;

            Date[2] = 0;

            _KolZap++;

        }


        public String Famil
        {

            get { return _famil; }

            set { _famil = value; }

        }



        public String Name
        {

            get { return _name; }

            set { _name = value; }

        }

        public String Telefon
        {
            get { return _telefon; }

            set { _telefon = value; }

        }

        public int[] Date
        {

            get { return _date; }

            set { _date = value; }

        }

        public static int GetKolZap()
        {

            return _KolZap;

        }


        public void print()
        {

            Console.WriteLine("{0} {1}, родился {2}.{3}.{4}, его номер телефона:{5}", Name, Famil, Date[0], Date[1], Date[2], Telefon);

        }
    }
}

