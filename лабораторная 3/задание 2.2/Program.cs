﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace задание_2._2
{
    class Program
    {
        static double f(double x)
        {
            if (Math.Abs(x) < 3)
            {
                return Math.Sin(x * Math.PI / 180);
            }
            else if (3 <= Math.Abs(x) && Math.Abs(x) < 9)
            {
                return Math.Sqrt(Math.Pow(x, 2) + 1) / Math.Sqrt(Math.Pow(x, 2) + 5);
            }
            else
            {
                return Math.Sqrt(Math.Pow(x, 2) + 1) - Math.Sqrt(Math.Pow(x, 2) + 5);
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Введите a:");
            double a = double.Parse(Console.ReadLine());
            Console.WriteLine("Введите b:");
            double b = double.Parse(Console.ReadLine());
            Console.WriteLine("Введите h:");
            double h = double.Parse(Console.ReadLine());
            Console.WriteLine("X Y");
            for (double x = a; x <= b; x += h)
            {
                Console.WriteLine(x + " " + f(x));
                {
                }
            }
        }
    }
}