﻿using System;

namespace Lab6
{
    class Program
    {
        static void Weight(int[] a)
        {
            Random rnd = new Random();
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = rnd.Next(50, 101);
                Console.Write("{0} ", a[i]);
            }
            Console.WriteLine();
        }

        static void Change1(int[] a)
        {
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = a[i] * 2;
            }
        }

        static void Change2(int[] a)
        {
            int b = a[1];
            for (int i = 0; i < a.Length; i++)
            {                
                a[i] = a[i] / b;
            }
        }

        static void Change3(int[] a, int A)
        {
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = a[i] - A;
            }
        }

        static void Print(int[] a)
        {
            for (int i = 0; i < a.Length; i++)
            {
                Console.Write("{0} ", a[i]);
            }
            Console.WriteLine();
        }

        static void Enter_1(int[] a)
        {
            Random rnd = new Random();
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = rnd.Next(100);
                Console.Write(a[i] + " ");
            }
            
        }

        static void F3_a(int[] a)
        {
            int sum = 0;
            for (int i = 1; i < a.Length; i++)
            {
                sum = sum + a[i];
            }
            if (sum % 2 == 0)
                Console.Write("Верно.");
            else
                Console.Write("Неверно.");
            Console.WriteLine();
        }

        static void F3_b(int[] a)
        {
            double sum = 0;
            for (int i = 0; i < a.Length; i++)
            {
                sum = sum + Math.Pow(a[i], 2) ;
            }
            if (sum / 100000 < 1 && sum / 10000 > 1)
                Console.Write("Верно.");
            else
                Console.Write("Неверно.");
            Console.WriteLine();
        }

        static void F4 (int[] a)
        {
            int sum1 = 0, sum2 = 0;
            for (int i = 0; i < a.Length; i+=2)
            {
                sum1 = sum1 + a[i];
            }
            for (int i = 1; i < a.Length; i += 2)
            {
                sum2 = sum2 + a[i];
            }
            if (sum1 > sum2)
                Console.WriteLine("На нечетной стороне");
            else
                Console.WriteLine("На четной стороне");
        }

        static void Enter_2(int[] a)
        {
            Random rnd = new Random();
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = rnd.Next(-100, 100);
                while (a[i] == 0)
                    a[i] = rnd.Next(-100, 100);
                Console.Write(a[i] + " ");
            }
            Console.WriteLine();

        }

        static void F5 (int[] a)
        {
            int value = 0;
            int mark = a[0];
            for (int i = 1; i < a.Length; i++)
            {
                if (mark < 0)
                {
                    if (mark * a[i] < 0)
                        value += 1;
                }
                if (mark > 0)
                {
                    if (mark * a[i] < 0)
                        value += 1;
                }
                mark = a[i];
            }
            Console.WriteLine(value) ;
        }


        static void Main(string[] args)
        {
            //ЗАДАЧА 1
            Console.WriteLine("Задача 1");
            int[] weight = new int[20];
            Weight(weight);


            Console.WriteLine();

            //ЗАДАЧА 2
            Console.WriteLine("Задача 2");
            int[] mas = new int[10];
            for (int i = 0; i < mas.Length; i++)
            {
                mas[i] = 2;
                Console.Write("{0} ", mas[i]);
            }
            Console.WriteLine();
            Change1(mas);
            Console.Write("А) ");
            Print(mas);
            Change2(mas);            
            Console.Write("Б) ");
            Print(mas);
            Console.WriteLine("Введите число на которое нужно уменьшить массив:");
            int A = int.Parse(Console.ReadLine());
            Change3(mas, A);
            Console.Write("В) ");
            Print(mas);

            Console.WriteLine();

            //ЗАДАЧА 3
            Console.WriteLine("Задача 3");
            int[] mas2 = new int[5];
            Enter_1(mas2);
            Console.WriteLine();
            Console.Write("A)");
            F3_a(mas2);
            Console.Write("Б)");
            F3_b(mas2);

            Console.WriteLine();

            //ЗАДАЧА 4
            Console.WriteLine("Задача 4");
            int[] street = new int[10];
            Enter_1(street);
            Console.WriteLine();
            F4(street);

            Console.WriteLine();

            //ЗАДАЧА 5
            Console.WriteLine("Задача 5");
            int[] mas5 = new int[10];
            Enter_2(mas5);
            F5(mas5);
        }
    }
}
