﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace задание_6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("введите поочерёдно координаты вершин треугольника x1, y1, x2, y2, x3, y3");
            double x1 = double.Parse(Console.ReadLine());
            double y1 = double.Parse(Console.ReadLine());
            double x2 = double.Parse(Console.ReadLine());
            double y2 = double.Parse(Console.ReadLine());
            double x3 = double.Parse(Console.ReadLine());
            double y3 = double.Parse(Console.ReadLine());
            double p = Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2)) + Math.Sqrt(Math.Pow(x3 - x2,2) + Math.Pow(y3 - y2,2)) + Math.Sqrt(Math.Pow(x3 - x1,2) + Math.Pow(y3 - y1,2));
            Console.WriteLine("периметр треугольника равен" + p);

        }

    }
}
