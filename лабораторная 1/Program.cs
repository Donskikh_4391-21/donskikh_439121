﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("введите периметр треугольника");
            double p = double.Parse(Console.ReadLine());
            double s = Math.Sqrt(3) / 4 * Math.Pow(p / 3, 2);
            Console.WriteLine("площадь равна {0:f2}", s);
        }
    }
}