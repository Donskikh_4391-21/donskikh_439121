﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace задание_10
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("введите первый член прогрессии");
            double one = double.Parse(Console.ReadLine());
            Console.WriteLine("введите разность прогрессии");
            double raznost = double.Parse(Console.ReadLine());
            Console.WriteLine("введите количество членов прогрессии");
            double quantity = double.Parse(Console.ReadLine());
            double sum = Math.Pow(quantity * one + (quantity - 1) * raznost,2);
            Console.WriteLine("сумма членов прогрессии равна" +sum);

        }
    }
}
