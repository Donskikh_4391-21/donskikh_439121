﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static int value(string a)
        {
            int k = 0;
            for (int i = 0; i < a.Length; i++)
            {
                if (char.IsLetter(a[i]))
                    k++;
            }
            return k;
        }
        static void udalenie(StringBuilder a)
        {
            int k = 0;
            int n = 0;
            for (int i=0; i<a.Length;i++)
            {
                if (a[i] ==','&& i<k)
                {
                    n = i;
                }
            }
            for (int i = 0; i < n; i++)
                Console.Write(a[i]);
            for (int i = k + 1; i < a.Length; i++)
                Console.Write(a[i]);
        }
        static void Different(stringa a) 
        {
            int count = 0;
            for(int i=0; i<a.Length; i++)
            {
                int k = 1;
                for (int j = 0; j < i; j++)
                {
                    if (a[i] == a[j])
                        k = 0;
                }
                if (k > 0)
                    count++;
            }
            Console.WriteLine("Различных символов в строке-", count);
        }
        static void Main(string[] args)
        {

            // Задача1
            Console.WriteLine("ВВЕДИТЕ СТРОКУ");
            string s = Console.ReadLine();
            StringBuilder a = new StringBuilder(s);
            for (int i = 0; i < a.Length; i += 2)
            {
                char b = a[i];
                a[i] = a[i + 1];
                a[i + 1] = b;
            }
            Console.WriteLine("Получившаяся строчка: " + a);
            // Задача 2
            Console.WriteLine("Количество строк :" + value(s));
            // Задача 3
            bool k = false;
            for (int i = 0; i < a.Length - 1; i++)
            {
                if (s[i] == s[i + 1])
                {
                    k = true;
                    break;
                }
                if (k == true)
                    Console.WriteLine("Да, имеется");
                else Console.WriteLine("Нет, не имеется");
                // Задание 4
                Console.WriteLine("Введите строку");
                StringBuilder c1 = new StringBuilder(Console.ReadLine());

                if (c1.Length % 2 == 0)
                    c1.Remove(c1.Length / 2 - 1, 2);
                else c1.Remove(c1.Length / 2, 1);
                Console.WriteLine("Получишваеся строка : " + c1);
                // Задача 5
                Console.WriteLine("Введите строку");
                StringBuilder a1 = new StringBuilder(Console.ReadLine());
                Console.WriteLine("Введите старую подстроку");
                string substr1 = Console.ReadLine();
                Console.WriteLine("Введите новую подстроку");
                string substr2 = Console.ReadLine();
                a1.Replace(substr1, substr2);
                Console.WriteLine("Полученная строка: " + a1);
                // Задача 6
                Console.WriteLine("Введите строку");
                string s1 = Console.ReadLine();
                double sum = 0;
                for (int i = 0; i < s1.Length; i++)
                {
                    if (Char.IsNumber(s1[i]))
                    {
                        sum = sum + char.GetNumericValue(s1[i]);
                    }

                }
                Console.WriteLine("Полученная сумма :" + sum);
                // Задача 7
                Console.WriteLine("Введите строку");
                string str2 = Console.ReadLine();
                for (int i = 0; i < str2.IndexOf(":"); i++)
                {
                    Console.Write("{0},", str2[i]);
                }
                // Задача 8
                Console.WriteLine("Введите строку");
                string str3 = Console.ReadLine();
                for (int i = str3.LastIndexOf(":") + 1; i < str3.Length; i++)
                {
                    Console.Write("{0},", str3[i]);
                }
                // Задача 9
                Console.WriteLine("Введите строку");
                StringBuilder b = new string(Console.ReadLine());
                udalenie(b);
                // Задача 10
                Console.WriteLine("Введите строку");
                string b = new string(Console.ReadLine());
                Different(b);
            }
        }
    }
}

